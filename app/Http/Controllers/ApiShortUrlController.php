<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

class ApiShortUrlController extends Controller
{
    public function cutUrl(Request $request){

        $request->validate([
            'url' => ['required', 'string', 'url'],
        ]);

        $urlCutter = $this->target_an_api_cutter($request->url);

        $response = [ 'url' => $request->url, 'urlCutter' => $urlCutter ];

        return response($response, 201);

    }

    public function target_an_api_cutter(String $url){
        $apiTinyurl = 'https://tinyurl.com/api-create.php?url=';

        $urlConcatenated = $apiTinyurl.$url;
        $responseApiTinyurl = Http::get($urlConcatenated);

        return $responseApiTinyurl->body();
    }
}
