<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use Illuminate\Support\Facades\Http;
use App\Models\User;

class ApiCreateTest extends TestCase
{
    use RefreshDatabase;


    private $urlToSend =  'https://www.20minutos.es/tecnologia/ciberseguridad/como-saber-si-un-enlace-acortado-es-seguro-antes-de-hacer-clic-en-el-4979779/';
    private $urlCutter;

    public function test_create_api()
    {

        $user = User::factory()->create([
            'email' => 'zmacayo@gmail.com',
            'name' => 'Ziuling Macayo'
        ]);

        $response = $this->post('/api/login', 
                    ['email' => $user->email,
                    'password' => 'password']);

        $tokenUser = $response->json('token');

        $response = $this->withHeader('Authorization', "Bearer {$tokenUser}")
                    ->post('/api/v1/short-urls', ['url' => $this->urlToSend ]);


        $response->assertStatus(201)
            ->assertJsonIsObject();

        /* con la api url correcta y sus validaciones hacemos el uso de la api Tinyurl */
        $this->target_an_api_cutter();

        //comprobar que la respuesta trae la url cortada
        $this->assertEquals($this->urlCutter, $response['urlCutter']);

    }

    public function target_an_api_cutter(){
        $apiTinyurl = 'https://tinyurl.com/api-create.php?url=';
        $urlConcatenated = $apiTinyurl.$this->urlToSend;
        $responseApiTinyurl = Http::get($urlConcatenated);

        $this->urlCutter = $responseApiTinyurl->body();
    }
}
