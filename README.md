
## Prueba técnica de un proceso de selección

Acontinuación detallo mediante commits los pasos que seguí al desarrollar esta prueba técnica que consiste en crear una api de cortador de urls consumiendo a su vez una api que corte las urls, en este caso ha sido Tinyurl, intenté usar la técnica TDD ya que no tengo experiencia laboral y poco conocimiento sobre ello pero investigando un poco he logrado seguir adelante con esta prueba.

Cabe destacar que en mi día a día mis desarrollos son en ramas específicas por tarea asignada, cuya rama la creo desde la rama de desarrollo y no desde master, aquí directamente he trabajado sobre master porque me di cuenta tarde que no habia creado una rama dev me disculpo de antemano por eso. 

Espero que pueda ser de agrado. Un saludo.